# typed: false
# frozen_string_literal: true

class IsynthCli < Formula
    desc "single binary cli for remote control of the best tool for synthetic data"
    homepage "https://isynth.io"
    version "1.12.0"
    license "proprietary"
  
    # depends_on "helm"
    # depends_on "kubernetes-cli"
  
    on_macos do
      if Hardware::CPU.arm?
        url "https://gitlab.com/api/v4/projects/54149759/packages/generic/isynth-cli/v1.12.0/isynth-cli-v1.12.0-mac-arm"
        sha256 "ddb57d22044a65aea722cf0b806cb39994ca8b56e544ce7133f4c1c3f33daf56"
  
        def install
          bin.install "isynth-cli-v1.12.0-mac-arm" => "isynth-cli"
        end
      end
      if Hardware::CPU.intel?
        url "https://gitlab.com/api/v4/projects/54149759/packages/generic/isynth-cli/v1.12.0/isynth-cli-v1.12.0-mac"
        sha256 "28237f05d00ab554e8625be32558e05086d140ac4f35147a546508896897ede1"
  
        def install
          bin.install "isynth-cli-v1.12.0-mac" => "isynth-cli"
        end
      end
    end
  
    on_linux do
    #   if Hardware::CPU.arm? && Hardware::CPU.is_64_bit?
    #     url "https://gitlab.com/api/v4/projects/54149759/packages/generic/isynth-cli/v1.11.0/isynth-cli-v1.11.0-linux"
    #     sha256 "70a3b186841a9592616bbfda3699e0b6f158a574719fbb50efa02d33afad281e"
  
    #     def install
    #       bin.install "isynth-cli"
    #     end
    #   end
      if Hardware::CPU.intel?
        url "https://gitlab.com/api/v4/projects/54149759/packages/generic/isynth-cli/v1.12.0/isynth-cli-v1.12.0-linux"
        sha256 "886299aa2e48792102aa1293cce958bfba9be9b6dcdc9bf51e223698de3e8f91"
  
        def install
          bin.install "isynth-cli-v1.12.0-linux" => "isynth-cli"
        end
      end
    end
  end